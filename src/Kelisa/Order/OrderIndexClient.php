<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Kelisa\Order;

/**
 * 提供订单索引服务数据
 */
class OrderIndexClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * 通过订单ID获取订单列表
     * @param \Kelisa\Order\GetOrderListByIdRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetOrderListById(\Kelisa\Order\GetOrderListByIdRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/kelisa.order.OrderIndex/GetOrderListById',
        $argument,
        ['\Kelisa\Order\OrderListResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * 通过更新时间获取订单列表
     * @param \Kelisa\Order\GetOrderListByUpdatedTimeRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetOrderListByUpdatedTime(\Kelisa\Order\GetOrderListByUpdatedTimeRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/kelisa.order.OrderIndex/GetOrderListByUpdatedTime',
        $argument,
        ['\Kelisa\Order\OrderListResponse', 'decode'],
        $metadata, $options);
    }

}
