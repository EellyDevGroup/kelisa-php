<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Kelisa\Live;

/**
 */
class LiveGoodsClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * 检查商品是否直播商品
     * @param \Kelisa\Live\GoodsIdList $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function CheckIsLiveGoods(\Kelisa\Live\GoodsIdList $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/kelisa.live.LiveGoods/CheckIsLiveGoods',
        $argument,
        ['\Kelisa\Live\IsLiveGoodsResponse', 'decode'],
        $metadata, $options);
    }

}
