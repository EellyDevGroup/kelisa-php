<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Kelisa\Live;

/**
 */
class LiveClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Kelisa\Live\StoreListRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function GetStoreList(\Kelisa\Live\StoreListRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/kelisa.live.Live/GetStoreList',
        $argument,
        ['\Kelisa\Live\StoreListResponse', 'decode'],
        $metadata, $options);
    }

}
