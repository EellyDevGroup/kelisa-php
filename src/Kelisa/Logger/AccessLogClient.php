<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Kelisa\Logger;

/**
 * 访问日志接口
 */
class AccessLogClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * 添加日志
     * @param \Kelisa\Logger\AccessLogRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function AddLog(\Kelisa\Logger\AccessLogRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/kelisa.logger.AccessLog/AddLog',
        $argument,
        ['\Google\Protobuf\BoolValue', 'decode'],
        $metadata, $options);
    }

}
