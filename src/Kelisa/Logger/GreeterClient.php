<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Kelisa\Logger;

/**
 * The greeting service definition.
 */
class GreeterClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * Sends a greeting
     * @param \Kelisa\Logger\HelloRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function SayHello(\Kelisa\Logger\HelloRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/kelisa.logger.Greeter/SayHello',
        $argument,
        ['\Kelisa\Logger\HelloReply', 'decode'],
        $metadata, $options);
    }

}
