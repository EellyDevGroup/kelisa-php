<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: logger/access_log.proto

namespace Kelisa\Logger;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>kelisa.logger.RequestOauth</code>
 */
class RequestOauth extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string oauthAccessTokenId = 1;</code>
     */
    protected $oauthAccessTokenId = '';
    /**
     * Generated from protobuf field <code>string oauthClientId = 2;</code>
     */
    protected $oauthClientId = '';
    /**
     * Generated from protobuf field <code>string oauthUserId = 3;</code>
     */
    protected $oauthUserId = '';
    /**
     * Generated from protobuf field <code>repeated string oauthScopes = 4;</code>
     */
    private $oauthScopes;
    /**
     * Generated from protobuf field <code>repeated string oauthStores = 5;</code>
     */
    private $oauthStores;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $oauthAccessTokenId
     *     @type string $oauthClientId
     *     @type string $oauthUserId
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $oauthScopes
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $oauthStores
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Logger\AccessLog::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string oauthAccessTokenId = 1;</code>
     * @return string
     */
    public function getOauthAccessTokenId()
    {
        return $this->oauthAccessTokenId;
    }

    /**
     * Generated from protobuf field <code>string oauthAccessTokenId = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setOauthAccessTokenId($var)
    {
        GPBUtil::checkString($var, True);
        $this->oauthAccessTokenId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string oauthClientId = 2;</code>
     * @return string
     */
    public function getOauthClientId()
    {
        return $this->oauthClientId;
    }

    /**
     * Generated from protobuf field <code>string oauthClientId = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setOauthClientId($var)
    {
        GPBUtil::checkString($var, True);
        $this->oauthClientId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string oauthUserId = 3;</code>
     * @return string
     */
    public function getOauthUserId()
    {
        return $this->oauthUserId;
    }

    /**
     * Generated from protobuf field <code>string oauthUserId = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setOauthUserId($var)
    {
        GPBUtil::checkString($var, True);
        $this->oauthUserId = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated string oauthScopes = 4;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getOauthScopes()
    {
        return $this->oauthScopes;
    }

    /**
     * Generated from protobuf field <code>repeated string oauthScopes = 4;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setOauthScopes($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->oauthScopes = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated string oauthStores = 5;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getOauthStores()
    {
        return $this->oauthStores;
    }

    /**
     * Generated from protobuf field <code>repeated string oauthStores = 5;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setOauthStores($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->oauthStores = $arr;

        return $this;
    }

}

