<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) blty.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Kelisa\Logger;

use Grpc;
use PHPUnit\Framework\TestCase;

class GreeterClientTest extends TestCase
{
    private $greeterClient;

    protected function setUp(): void
    {
        $this->greeterClient = new GreeterClient('127.0.0.1:50051', [
            'credentials'                   => Grpc\ChannelCredentials::createSsl(file_get_contents(\dirname(__DIR__).'/cert/server_develop.crt')),
            'grpc.ssl_target_name_override' => 'eelly.com',
        ]);
    }

    protected function tearDown(): void
    {
        $this->greeterClient->close();
    }

    public function testSayHello(): void
    {
        [$response, $status] = $this->greeterClient->SayHello(new HelloRequest(['name' => 'eelly(衣联)']))->wait();
        /* @var $response \Kelisa\Logger\HelloReply */
        dump($response->getMessage(), $status);
    }
}
