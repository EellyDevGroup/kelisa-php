<?php

declare(strict_types=1);

/*
 * This file is part of eelly package.
 *
 * (c) blty.com
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Logger;

use Grpc;
use Kelisa\Logger\AccessLogClient;
use Kelisa\Logger\AccessLogRequest;
use PHPUnit\Framework\TestCase;

class AccessLogClientTest extends TestCase
{
    private $accessLogClient;

    protected function setUp(): void
    {
        $this->accessLogClient = new AccessLogClient('127.0.0.1:50051', [
            'credentials'                   => Grpc\ChannelCredentials::createSsl(file_get_contents(\dirname(__DIR__).'/cert/server_develop.crt')),
            'grpc.ssl_target_name_override' => 'eelly.com',
        ]);
    }

    protected function tearDown(): void
    {
        $this->accessLogClient->close();
    }

    public function testAddLog(): void
    {
        $accessLogRequest = new AccessLogRequest();
        [$response, $status] = $this->accessLogClient->AddLog($accessLogRequest)->wait();
        /* @var $response \Kelisa\Logger\BoolResponse */
        dump($response->getResult(), $status);
    }
}
